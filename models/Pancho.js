const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');

let Schema = mongoose.Schema;

let panchoSchema = new Schema({
  name: {
    type: String,
    //required: [true, 'The name is required.']
  },
  reason: {
    type: String,
    default: 'For some reason this happened'
  },
  active: {
    type: Boolean,
    default: true
  },
  dateMoment: {
    type: String,
    default: moment(new Date()).format("MM-DD-YYYY")
  },
  date: {
    type: String,
    default: moment().format("MMM Do YY")
  },
  dateArchived: {
    type: String,
    default: ''
  },

});

// Apply the uniqueValidator plugin to panchoSchema.
panchoSchema.plugin(uniqueValidator, { message: 'The {PATH} should be unique.' });

module.exports = mongoose.model('Pancho', panchoSchema);