const moment = require('moment');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

//Create User Schema
const UserSchema = new Schema({
    user_name: {
        type: String,
        unique: true,
        required: [true, 'The user name is required']
    },
    password: {
        type: String,
        required: [true, 'The password is required']
    },
    date: {
        type: String,
        default: moment(new Date()).format("MM-DD-YYYY")
    }
});

//hide the password

UserSchema.methods.toJSON = function () {
    let user = this;
    let userObject = user.toObject();

    delete userObject.password;

    return userObject;
}

// Apply the uniqueValidator plugin to UserSchema.
UserSchema.plugin(uniqueValidator, { msg: 'The {PATH} must be unique.'});

module.exports = User = mongoose.model('user', UserSchema);