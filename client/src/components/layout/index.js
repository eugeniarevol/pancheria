export { default as Error } from "./Error";
export { default as Footer } from "./Footer";
export { default as Header } from "./Header";
export { default as Home } from "./Home";
export { default as PanchosList } from "./PanchosList";
export { default as PanchoForm } from "./PanchoForm";