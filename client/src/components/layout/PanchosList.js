import { hotdog1, hotdog2, hotdog3 } from '../../img/status';
import classNames from 'classnames';
import moment from 'moment';
import React, { Component } from 'react';

class PanchosList extends Component {

  render() {
    return (
      <div className="row">
        <ul>
          {this.props.panchos.map((pancho) =>
            <div className="col l4 m6 s12" key={pancho._id}>
              <li>
                <div className="card home-card hoverable">
                  <div className={this.getPanchoColor(pancho)}>
                    {this.renderStatusHotDog(pancho)}
                    <span className="card-title">{pancho.name}</span>
                    <button onClick={() => this.props.handleArchive(pancho._id)} className="btn-floating halfway-fab waves-effect waves-light red" type="submit" name="action">
                      <i className="material-icons">delete</i>
                    </button>
                  </div>
                  <div className="card-content home-card-content">
                    <p className="font-bold">{pancho.date.toUpperCase()}</p>
                    <p className="truncate">{pancho.reason}</p>
                  </div>
                </div>
              </li>
            </div>
          )}
        </ul>
      </div>
    );
  }

  getPanchoColor = (pancho) => {
    let statusColor = this.getStatus(pancho);

    let getCardClass = classNames(
      'card-image home-card-pancho',
      'status-color' + statusColor
    );

    return getCardClass;
  }

  renderStatusHotDog(pancho) {
    let statusColor = this.getStatus(pancho);
    var dataToRender = null;
    switch (statusColor) {
      case 1:
        dataToRender = <img className="home-card-img" alt="pancheria" src={hotdog1} />
        break;
      case 2:
        dataToRender = <img className="home-card-img" alt="pancheria" src={hotdog2} />
        break;
      case 3:
        dataToRender = <img className="home-card-img" alt="pancheria" src={hotdog3} />
        break;

      default:
        dataToRender = <img className="home-card-img" alt="pancheria" src={hotdog1} />
        break;
    }

    return dataToRender;
  }

  getStatus = (pancho) => {
    let created = moment(pancho.dateMoment, "MM-DD-YYYY");
    let now = moment();
    let daysBetween = Math.floor(moment.duration(now.diff(created)).asDays());
    let status = null;

    if (daysBetween > 14) {
      status = 3;
    } else if (daysBetween > 7) {
      status = 2;
    } else {
      status = 1;
    }
    return status;
  }
}
export default PanchosList;