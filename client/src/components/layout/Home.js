import { connect } from 'react-redux';
import { ENV } from '../../config/config';
import { getRandomGif } from '../../img/lol';
import { loginUser } from '../../actions/authActions';
import axios from 'axios';
import nyancat from '../../img/nyan-cat.gif';
import PanchoForm from './PanchoForm';
import PanchosList from './PanchosList';
import patternMono from '../../img/pattern-mono-2.jpg';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import swal from 'sweetalert2';

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      errors: {},
      password: '',
      userName: '',
      panchos: [],
      loading: false
    }
  }

  componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push('/');
    }
    this.getPanchos();
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.auth.isAuthenticated) {
      this.props.history.push('/');
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  getPanchos = () => {
    this.setState({
      loading: true
    })
    axios.get(`${ENV}/api/panchos`)
      .then(resp => {
        this.setState({
          panchos: resp.data,
          loading: false
        })
      })
      .catch(err => console.log(`ERROR - ${err}`))
  }

  postPancho = (body) => {
    let gif = getRandomGif();
    this.setState({
      loading: true
    })
    axios.post(`${ENV}/api/panchos`, body)
      .then(resp => {
        let pancho = resp.data.pancho;
        this.setState(prevState => ({
          panchos: [...prevState.panchos, pancho],
          loading: false
        }));
      })
      .then(
        swal({
          title: '<strong>Great!</strong>',
          html: '<strong>Your pancho has been saved.</strong>',
          type: 'success',
          confirmButtonColor: '#2A3482',
          width: 600,
          padding: '3em',
          background: `#fff url("${patternMono}")`,
          backdrop: `
        rgba(0,0,0,0.7)
        url("${gif}")
        bottom right
        no-repeat
      `
        }))
      .catch(err => console.log(`ERROR - ${err}`))
  }

  handleSubmit = body => {
    if (body) {
      this.postPancho(body);
    }
  }

  handleArchive = (id) => {
    swal({
      title: '<strong>Are you sure?</strong>',
      html: '<strong>You won\'t be able to revert this!</strong>',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2A3482',
      cancelButtonColor: '#e65100',
      confirmButtonText: 'Yes, archive it!',
      width: 600,
      padding: '3em',
      background: `#fff url("${patternMono}")`,
      backdrop: `
        rgba(0,0,0,0.7)
        url("${nyancat}")
        center left
        no-repeat
      `
    }).then((result) => {
      if (result.value) {
        this.setState({
          loading: true
        })
        axios.put(`${ENV}/api/panchos/archive/${id}`)
          .then(resp => {
            if (resp.status === 200) {
              const panchos = [...this.state.panchos];
              let result = panchos.filter(pancho => (
                pancho._id !== id
              ));
              this.setState({
                panchos: result,
                loading: false
              })
            }
          })
          .then(swal({
            title: '<strong>Archived!</strong>',
            html: '<strong>Your pancho has been archived.</strong>',
            type: 'success',
            confirmButtonColor: '#2A3482',
            width: 600,
            padding: '3em',
            background: `#fff url("${patternMono}")`
          }))
          .catch(err => console.log(`ERROR - ${err}`))

      }
    })
  }

  render() {
    const { errors } = this.state;
    return (
      <div className="container">
        <PanchoForm handleSubmit={this.handleSubmit} />
        {this.state.loading ? this.renderLoading() : null}
        <PanchosList {...this.getPanchosListProps()} />
      </div>
    );
  }

  renderLoading = () => {
    return (
      <div className="progress">
        <div className="indeterminate"></div>
      </div>
    );
  }

  getPanchosListProps = () => {
    return {
      panchos: this.state.panchos,
      handleArchive: this.handleArchive
    }
  }
}

Home.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Home);