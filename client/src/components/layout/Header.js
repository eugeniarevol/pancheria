import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';
import hotdog from '../../img/hot-dog-color.png';
import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';

class Header extends Component {

  onLogoutClick = (e) => {
    e.preventDefault();
    this.props.logoutUser();
  }

  render() {
    const { isAuthenticated, user } = this.props.auth;
    const authLinks = (
      <ul className="right">
        <li className="mr10 hide-on-med-and-down"><span><i className="material-icons left">account_circle</i>Welcome, {user.user_name}!</span></li>
        <li><a href="" onClick={this.onLogoutClick}>Logout</a></li>
      </ul>
    );

    return (
      <nav className="indigo mb20">
        <div className="nav-wrapper">
          <img className="header-logo-img hide-on-med-and-down" alt="pancheria" src={hotdog} />
          <a href="index.html" className="brand-logo">Pancheria</a>
          {isAuthenticated ? authLinks : null}
        </div>
      </nav>
    );
  }
}

Header.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logoutUser })(Header);