import React, { Component } from 'react';

class Error extends Component {
  render() {
    return (
      <div className="row">
        <div className="col s12">
          <div className="card-panel red lighten-4">
            <span className="red-text text-accent-4">Oops! there was an error...!</span>
          </div>
        </div>
      </div>
    );
  }
}

export default Error;