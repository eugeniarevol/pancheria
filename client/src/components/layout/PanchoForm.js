import React, { Component } from 'react';

class PanchoForm extends Component {

  nameInputRef = React.createRef();
  reasonInputRef = React.createRef();

  onKeyPress = (e) => {
    if (e.which === 13) {
      e.preventDefault();
      this.postPancho(e);
    }
  }

  postPancho = (e) => {
    e.preventDefault();

    const body = {
      name: this.nameInputRef.current.value,
      reason: this.reasonInputRef.current.value
    }
    
    this.nameInputRef.current.value = '';
    this.reasonInputRef.current.value = '';

    this.props.handleSubmit(body);
  }

  render() {
    return (
      <div className="row">
        <form className="col s12" onSubmit={this.postPancho}>
          <div className="row">
            <div className="input-field col s12 l3">
              <i className="material-icons prefix">person</i>
              <input ref={this.nameInputRef} id="name" type="text" className="validate" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="input-field col s12 l7">
              <i className="material-icons prefix">short_text</i>
              <input ref={this.reasonInputRef} id="reason" type="text" className="validate" />
              <label htmlFor="reason">Reason</label>
            </div>
            <div className="col s12 l2">
              <button className="btn-large col s12 waves-effect waves-light orange darken-4" 
                type="submit" name="action">Pancho!
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default PanchoForm;