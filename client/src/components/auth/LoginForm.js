import { connect } from 'react-redux';
import { loginUser } from '../../actions/authActions';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

class LoginForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      errors: {},
      password: '',
      loading: false,
      userName: ''
    }
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/board');
      this.setState({
        loading: false
    });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push('/board');
      this.setState({
        loading: false
    });
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors, loading: false });
    }
  }

  onSubmit = (e) => {
    e.preventDefault();

    const userData = {
      user_name: this.state.userName,
      password: this.state.password
    }

    this.setState({
        loading: true
    })

    this.props.loginUser(userData);
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  render() {
    const { errors } = this.state;

    return (
      <center>
        <h5 className="indigo-text mt50">Please, login into your account</h5>
        <div className="container mt50">
          <div className="z-depth-1 grey lighten-4 row login-form-card">
            <form className="col s12" onSubmit={this.onSubmit}>
              <div className='row'>
                <div className='col s12'>
                </div>
              </div>
              <div className='row'>
                <div className='input-field col s12'>
                  <input onChange={this.onChange}
                    className={classNames({ 'invalid': errors.user_name })}
                    type='text' value={this.state.userName} name='userName' id='userName' />
                  <label htmlFor='userName'>Enter your user name</label>
                  {errors.user_name && (<span className="helper-text" data-error={errors.user_name}></span>)}
                </div>
              </div>
              <div className='row'>
                <div className='input-field col s12'>
                  <input onChange={this.onChange}
                    className={classNames({ 'invalid': errors.password })}
                    type='password' name='password' value={this.state.password} id='password' />
                  <label htmlFor='password'>Enter your password</label>
                  {errors.password && (<span className="helper-text" data-error={errors.password}></span>)}
                </div>
                <label style={{ float: 'right' }}>
                  <span className='pink-text'><b>Forgot Password?</b></span>
                </label>
              </div>
              <center>
                <div className='row'>
                    {this.state.loading ? this.renderLoading() : <button type='submit' name='btn_login' className='col s12 btn btn-large waves-effect waves-light indigo'>Login</button>}
                </div>
              </center>
            </form>
          </div>
        </div>
        <span className='blue-text'>Create account</span>
      </center>
    );
  }

  renderLoading = () => {
    return (
      <div className="progress">
        <div className="indeterminate"></div>
      </div>
    );
  }
}

LoginForm.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(LoginForm);