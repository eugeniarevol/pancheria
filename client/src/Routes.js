import './css/main.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Error, Header, Footer, Home } from './components/layout';
import { LoginForm } from './components/auth';
import { Provider } from 'react-redux';
import { setCurrentUser, logoutUser } from './actions/authActions';
import jwt_decode from 'jwt-decode';
import React, { Component, Fragment } from 'react';
import setAuthToken from './utils/setAuthToken';
import store from './store'

// Check for toke
if (localStorage.jwtToken) {
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Redirect to login
    window.location.href = '/';
  }
}

class Routes extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Fragment>
            <Header />
            <div>
              <main>
                <Switch>
                  <Route path='/' exact render={props => <LoginForm {...props} />} />
                  <Route path='/board' exact render={props => <Home {...props} />} />
                  <Route component={Error} />
                </Switch>
              </main>
            </div>
            <Footer />
          </Fragment>
        </Router>
      </Provider>
    );
  }
}

export default Routes;
