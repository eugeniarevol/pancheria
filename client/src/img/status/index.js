const hotdog1 = require('./hot-dog-color.png');
const hotdog2 = require('./hot-dog-color-bad.png');
const hotdog3 = require('./hot-dog-color-very-bad.png');

module.exports = {
  hotdog1,
  hotdog2,
  hotdog3
}