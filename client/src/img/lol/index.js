const lol1 = require('./lol1.gif');
const lol2 = require('./lol2.gif');
const lol3 = require('./lol3.gif');
const lol4 = require('./lol4.gif');

function getRandomGif() {
  const gifs = [lol1, lol2, lol3, lol4];
  // pick a random file from the list
  const index = Math.floor(Math.random() * gifs.length);

  return gifs[index];
}

module.exports = {
  getRandomGif
}