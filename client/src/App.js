import React, { Component, Fragment } from 'react';
import Routes from './components/Routes';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Routes />
      </Fragment>
    );
  }
}

export default App;
