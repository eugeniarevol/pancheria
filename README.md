# Pancho App

## Endpoints

* Get all panchos: ```/api/panchos```
* Post a pancho: ```/api/panchos```
* Update a pancho: ```/api/panchos/:id```
* Archive a pancho: ```/api/panchos/archive/:id```