const Validator = require('validator');
const _ = require('lodash');

module.exports = function validateRegisterInput(data) {
  let errors = {};
  
  data.user_name = !_.isEmpty(data.user_name) ? data.user_name : '';
  data.password = !_.isEmpty(data.password) ? data.password : '';

  if (!Validator.isLength(data.user_name, { min: 5, max: 15 })) {
    errors.user_name = 'User name must be between 5 and 15 characters';
  }

  if (Validator.isEmpty(data.user_name)) {
    errors.user_name = 'User name field is required';
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = 'Password field is required';
  }

  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Password must be at least 6 and less than 30 characters';
  }
  
  return {
    errors,
    isValid: _.isEmpty(errors)
  };
};
