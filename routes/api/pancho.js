const express = require('express');
const moment = require('moment');
const router = express.Router();

// Load Pancho Model
const Pancho = require('../../models/Pancho');

// @route   GET api/panchos
// @desc    Get all panchos
// @access  Public
router.get('/', (req, res) => {
  const errors = {};

  Pancho.find()
      .then(panchos => {
          if (!panchos) {
              errors.nopancho = 'There are no panchos';
              return res.status(404).json(errors);
          }

          res.json(panchos.filter(pancho => (
            pancho.active == true 
          )));
      })
      .catch(err => res.status(404).json({ pancho: 'There are no panchos' }));
});

// @route   POST api/panchos
// @desc    Post a pancho
// @access  Public
router.post('/', (req, res) => {
  let body = req.body;
    let pancho = new Pancho({
        name: body.name,
        reason: body.reason,
        active: body.active
    });
    //save pancho in database or err log
    pancho.save((err, panchoDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            pancho: panchoDB
        });
        console.log('New pancho posted!!!');
    })
});

// @route   PUT api/panchos/archive/:id
// @desc    Archive a pancho by changing state
// @access  Public
router.put('/archive/:id', (req, res) => {
  let id = req.params.id;
  let body = {
    active: false,
    dateArchived: moment().format("MMM Do YY")
  };
  Pancho.findByIdAndUpdate(id, body, (err, panchoDB) => {
      if (err) {
          return res.status(400).json({
              ok: false,
              err
          });
      }

      res.json({
          ok: true,
          pancho: panchoDB
      });
      console.log('Pancho archived!!!');
  })
});

module.exports = router;