require('../../config/config');
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');

// Load Input Validation
const validateInput = require('../../validation/validator');

// Load User model
const User = require('../../models/User');

// @route   POST api/user/register
// @desc    Register a user
// @access  Public
router.post('/register', (req, res) => {
  let body = req.body;
  const { errors, isValid } = validateInput(req.body);

  // Check Validation
  if (!isValid) {
      return res.status(400).json(errors);
  }

  User.findOne({ user_name: body.user_name })
      .then(user => {
          if (user) {
              errors.user_name = 'User name already exists';
              return res.status(400).json(errors)
          } else {
              const newUser = new User({
                  user_name: body.user_name,
                  password: body.password
              });

              bcrypt.genSalt(10, (err, salt) => {
                  bcrypt.hash(newUser.password, salt, (err, hash) => {
                      if (err) throw err;
                      newUser.password = hash;
                      newUser.save()
                          .then(user => res.json(user))
                          .catch(err => console.log(err));
                  })
              })
          }
      })
})

// @route   POST api/user/login
// @desc    Login User / Returning JWT Token
// @access  Public
router.post('/login', (req, res) => {
  const { errors, isValid } = validateInput(req.body);

  // Check Validation
  if (!isValid) {
      return res.status(400).json(errors);
  }

  const user_name = req.body.user_name;
  const password = req.body.password;

  // Find user by user_name
  User.findOne({ user_name }).then(user => {
      // Check for user
      if (!user) {
          errors.user_name = 'User not found';
          return res.status(404).json(errors);
      }

      // Check Password
      bcrypt.compare(password, user.password).then(isMatch => {
          if (isMatch) {
              // User Matched
              const payload = { id: user.id, user_name: user.user_name }; // Create JWT Payload
            
              // Sign Token
              jwt.sign(
                  payload,
                  process.env.SEED,
                  { expiresIn: '10m' }, //Expiration in 3 minutes
                  (err, token) => {
                      res.json({
                          success: true,
                          token: 'Bearer ' + token
                      });
                  }
              );
          } else {
              errors.password = 'Password incorrect';
              return res.status(400).json(errors);
          }
      });
  });
});

// @route   GET api/user/current
// @desc    Return current user
// @access  Private
router.get(
  '/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
      res.json({
          id: req.user.id,
          user_name: req.user.user_name
      });
  }
);

module.exports = router;